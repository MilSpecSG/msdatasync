package rocks.milspecsg.msdatasync;

public abstract class PluginPermissions {

    public static final String START_COMMAND = "msdatasync.command.admin.start.base";
    public static final String LOCK_COMMAND = "msdatasync.command.admin.lock.base";
    public static final String RELOAD_COMMAND = "msdatasync.command.admin.reload.base";

}
