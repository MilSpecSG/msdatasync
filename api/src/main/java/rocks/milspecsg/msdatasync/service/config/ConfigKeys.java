package rocks.milspecsg.msdatasync.service.config;

public abstract class ConfigKeys extends rocks.milspecsg.msrepository.api.config.ConfigKeys {

    public static final int ENABLED_SERIALIZERS_LIST = 1;
    public static final int SERIALIZE_ON_JOIN_LEAVE = 10;
    public static final int SERIALIZATION_TASK_INTERVAL_SECONDS = 11;
    public static final int MONGODB_HOSTNAME = 20;
    public static final int MONGODB_PORT = 21;
    public static final int MONGODB_DBNAME = 22;
    public static final int MONGODB_USERNAME = 23;
    public static final int MONGODB_PASSWORD = 24;
    public static final int MONGODB_USEAUTH = 25;

}
