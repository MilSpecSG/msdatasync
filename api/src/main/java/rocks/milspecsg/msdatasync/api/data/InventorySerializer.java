package rocks.milspecsg.msdatasync.api.data;

import rocks.milspecsg.msdatasync.model.core.Member;

public interface InventorySerializer<M extends Member, P> extends Serializer<M, P> {
}
