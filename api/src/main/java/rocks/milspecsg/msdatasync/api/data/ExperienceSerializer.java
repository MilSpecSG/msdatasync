package rocks.milspecsg.msdatasync.api.data;

import rocks.milspecsg.msdatasync.model.core.Member;

public interface ExperienceSerializer<M extends Member, P> extends Serializer<M, P> {
}
